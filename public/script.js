const socket = io('/');
const peer = new Peer(undefined, {
    host: '/',
    port: '3001'
});
var userId;
var users = [];

peer.on('open', id => {
    document.getElementById("userId").innerHTML = id;
    userId = id;
});

peer.on('call', call => {
    call.answer();
    call.on('stream', userVideoStream => {
        document.getElementById("shareScreen").style.display = "none";
        const video = document.createElement("video");
        video.id = "screen";
        video.srcObject = userVideoStream;
        video.addEventListener('loadedmetadata', () => {
            video.play();
        });
        document.getElementById("screenDiv").append(video);
    });
});


socket.on('user-connected', id => {
    if (!users.includes(id)){
        console.log("User connected: " + id);
        users.push(id);
        socket.emit('user-connected', ROOM_ID, userId);
    }
});


socket.on('mouse-move', (x, y) => {
    window.robotjs.wrapper.moveMouse(x, y);
});

socket.on('mouse-click', () => {
    window.robotjs.wrapper.mouseClick();
});


function makeRoom(){
    document.getElementById("roomId").innerHTML = ROOM_ID;
    socket.emit('join-room', ROOM_ID, userId);
    document.getElementById("shareScreen").style.display = "block";
}

function joinRoom(){
    ROOM_ID = document.getElementById("existingRoomId").value;
    document.getElementById("roomId").innerHTML = ROOM_ID;
    socket.emit('join-room', ROOM_ID, userId);
    document.getElementById("shareScreen").style.display = "block";
}

function shareScreen(){
    const video = document.createElement("video");
    navigator.mediaDevices.getDisplayMedia({
        video: true
    }).then(stream => {
        video.srcObject = stream;
        video.addEventListener('loadedmetadata', () => {
            video.play();
        });
        document.getElementById("screenDiv").append(video);

        users.forEach(userId => {
            peer.call(userId, stream);
        });
        document.getElementById("shareScreen").style.display = "none";
    });
}


document.getElementById('screenDiv').addEventListener("mousemove", function (e) {
    var x = e.pageX - this.offsetLeft;
    var y = e.pageY - this.offsetTop;

    socket.emit("mouse-move", x, y, ROOM_ID);
});

document.getElementById('screenDiv').addEventListener("click", function () {
    socket.emit("mouse-click", ROOM_ID);
});