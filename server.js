const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const { v4: uuidV4 } = require('uuid');

app.set('view engine', 'ejs');
app.use(express.static('public'));

app.get('/', (req, res) => {
    res.render('room', { roomId: uuidV4() });
});

io.on('connection', socket => {
    socket.on('join-room', (roomId, userId) => {
        socket.join(roomId);
        socket.to(roomId).emit('user-connected', userId);
    });

    socket.on('user-connected', (roomId, userId) => {
        socket.to(roomId).emit('user-connected', userId);
    });

    socket.on('mouse-move', (x, y, roomId) => {
        socket.to(roomId).emit('mouse-move', x, y);
    });

    socket.on('mouse-click', (roomId) => {
        socket.to(roomId).emit('mouse-click');
    });
});

server.listen(3000);