# Remote Access Control in a browser

This project was made for a schoolproject. In this project, it's possible to share your screen with everyone in the same room. All these users can also move your mouse and click.

## Installation

Use the package manager [npm](https://docs.npmjs.com/about-npm) to install all the required packages.

```bash
npm i express ejs socket.io
npm i uuid
npm i --save-dev nodemon
npm i -g peer
npm install robotjs
npm install -g robotjs-browser
```

## Usage

In the terminal, you should enter the following commands:

```bash
robotjs-browser -h
peerjs --port 3001
npm start
```
